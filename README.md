# README #

This is the Giant Bomb Enthusiast app for Fire TV and Android TV devices.

This is a project that was started in my spare time so I could watch Giant Bomb videos easily on my Fire TV device and learn about creating apps built on the Android Leanback library. Let's just say that some of the code in here was written in a fairly... casual manner. Try not to judge me too harshly.

Feel free to send a pull request if you have a bug fix, improvement, tests, etc. that you'd like to contribute.

You can contact me with any questions at gbenthusiast@gmail.com.

\- Alec Douglas