package com.alecgdouglas.gbenthusiast.model;

public class VideoType {
    private final String mName;
    private final long mId;
    private final String mDeck;

    public VideoType(String name, long id, String deck) {
        mName = name;
        mId = id;
        mDeck = deck;
    }

    public String getName() {
        return mName;
    }

    public long getId() {
        return mId;
    }

    public String getDeck() {
        return mDeck;
    }
}
