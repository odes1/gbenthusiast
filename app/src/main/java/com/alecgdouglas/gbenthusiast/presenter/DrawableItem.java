package com.alecgdouglas.gbenthusiast.presenter;

import android.support.v17.leanback.widget.ImageCardView;

public interface DrawableItem {

    public void drawItem(ImageCardView cardView, Object item);

}
