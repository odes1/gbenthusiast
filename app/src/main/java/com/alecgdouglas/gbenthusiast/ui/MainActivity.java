/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.alecgdouglas.gbenthusiast.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;

import com.alecgdouglas.gbenthusiast.R;

/*
 * MainActivity class that loads MainFragment
 */
public class MainActivity extends CommonActivity {
    public static final String ON_MENU_KEY_DOWN_BROADCAST_INTENT_KEY = "on_menu_key_down";
    public static final String ON_PLAY_PAUSE_KEY_DOWN_BROADCAST_INTENT_KEY =
            "on_play_pause_key_down";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            LocalBroadcastManager.getInstance(this)
                    .sendBroadcast(new Intent(ON_MENU_KEY_DOWN_BROADCAST_INTENT_KEY));
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
            LocalBroadcastManager.getInstance(this)
                    .sendBroadcast(new Intent(ON_PLAY_PAUSE_KEY_DOWN_BROADCAST_INTENT_KEY));
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
