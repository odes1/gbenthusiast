package com.alecgdouglas.gbenthusiast;

import android.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class ApiGetRequestParams {
    private static final String API_KEY_KEY = "api_key";
    private static final String FORMAT_KEY = "format";
    private static final String FORMAT_PARAM = "json";
    private final String mApi;
    private final Map<String, String> mParams = new HashMap<>();

    public ApiGetRequestParams(String apiKey, String api, Pair<String, String>... params) {
        this(api, params);
        mParams.put(API_KEY_KEY, apiKey);
    }

    /**
     * @param api    The API to query, e.g. 'videos'. See http://www.giantbomb.com/api/documentation
     * @param params Any params to be appended to the query. The pairs should have the first
     *               item be the key and the second item be the value. Null params will be ignored.
     */
    public ApiGetRequestParams(String api, Pair<String, String>... params) {
        mApi = api;

        for (Pair<String, String> param : params) {
            if (param == null) continue;
            mParams.put(param.first, param.second);
        }

        // Always request the specified format.
        mParams.put(FORMAT_KEY, FORMAT_PARAM);
    }

    public String getApi() {
        return mApi;
    }

    public String getFormattedParams() {
        StringBuilder formattedParams = new StringBuilder(mParams.size());
        for (Map.Entry<String, String> entry : mParams.entrySet()) {
            formattedParams.append(entry.getKey()).append("=").append(entry.getValue());
            formattedParams.append("&");
        }
        formattedParams.deleteCharAt(formattedParams.length() - 1);
        return formattedParams.toString();
    }

    public String getApiKey() {
        return mParams.get(API_KEY_KEY);
    }
}
