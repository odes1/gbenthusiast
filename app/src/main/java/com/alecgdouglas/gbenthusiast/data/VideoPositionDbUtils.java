package com.alecgdouglas.gbenthusiast.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.model.Video;

import java.util.Calendar;
import java.util.Locale;

public class VideoPositionDbUtils {
    public static final String ON_VIDEO_POSITION_UPDATED_BROADCAST_INTENT_KEY =
            "on_video_position_updated";
    public static final String VIDEO_KEY = "video";
    private static final String TAG = "VideoPositionDbUtils";

    private VideoPositionDbUtils() {
    }

    public static Long getCurrentPositionFromDbSync(Context context, long giantBombVideoId) {
        final ContentResolver contentResolver = context.getContentResolver();

        final String[] selectionProjection =
                {VideoContract.VideoPosition.COLUMN_VIDEO_POSITION_MILLIS};
        final String selectionClause = VideoContract.VideoPosition.COLUMN_VIDEO_ID + " = ?";
        final String[] selectionArgs = {String.valueOf(giantBombVideoId)};
        final Cursor selectionCursor = contentResolver
                .query(VideoContract.VideoPosition.CONTENT_URI, selectionProjection,
                        selectionClause, selectionArgs, null);

        if (selectionCursor == null || !selectionCursor.moveToFirst()) {
            return null;
        }
        final Long currentPosition = selectionCursor.getLong(selectionCursor
                .getColumnIndex(VideoContract.VideoPosition.COLUMN_VIDEO_POSITION_MILLIS));
        selectionCursor.close();
        return currentPosition;
    }

    public static void updateCurrentPosition(final Context context, final Video video,
                                             final long newPositionMillis) {
        if (video.isLive()) return;
        video.setPositionMillis(newPositionMillis);
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... unused) {
                final ContentResolver contentResolver = context.getContentResolver();
                final ContentValues values = new ContentValues();
                values.put(VideoContract.VideoPosition.COLUMN_VIDEO_POSITION_MILLIS,
                        video.getPositionMillis());
                values.put(VideoContract.VideoPosition.COLUMN_VIDEO_ID, video.getId());
                values.put(VideoContract.VideoPosition.COLUMN_LAST_UPDATED,
                        Calendar.getInstance(Locale.US).getTimeInMillis());
                final Uri insertedRow =
                        contentResolver.insert(VideoContract.VideoPosition.CONTENT_URI, values);
                return insertedRow != null;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) broadcastPositionUpdated(context, video);
            }
        }.execute();
    }

    public static void deleteSavedPosition(final Context context, final Video video) {
        if (video.isLive()) return;
        video.setPositionMillis(0L);
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... unused) {
                final String whereClause = VideoContract.VideoPosition.COLUMN_VIDEO_ID + " = ?";
                final String[] whereArgs = {String.valueOf(video.getId())};

                final ContentResolver contentResolver = context.getContentResolver();
                final int deleteCount = contentResolver
                        .delete(VideoContract.VideoPosition.CONTENT_URI, whereClause, whereArgs);

                Log.d(TAG, "Deleted " + deleteCount + " saved position(s) for video with id = " +
                        video.getId());
                return deleteCount >= 1;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) broadcastPositionUpdated(context, video);
            }
        }.execute();
    }

    private static void broadcastPositionUpdated(final Context context, final Video video) {
        final Intent intent = new Intent(ON_VIDEO_POSITION_UPDATED_BROADCAST_INTENT_KEY);
        intent.putExtra(VIDEO_KEY, video);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
